
# Clase 24

## Antes de correr el proyecto
Tenemos que tener mongodb instalado y corriendo

## Correr proyecto
```
yarn install
node index.js
```

<br>
La ruta para ver todo seria 

http://localhost:8080/

Dejo el postman para probar todas las rutas

Y arme una ruta extra para consultar el historial del chat 

GET localhost:8080/api/chat



Agrege 3 enpoints mas que son 

api/login Para logearse
api/logout Para deslogearse
api/me Para traer la informacion de la session

fetch("/api/me").then(function (response) {
    return response.json();
}).then(function (data) {
    if(data && Object.keys(data).length === 0){
        window.location = "/login.html"
        return
    }
    if (data.name) {
        document.getElementById("headerText").innerHTML = "Bienvenido " + data.name;
    }

}).catch(function (e) {
    console.log(e)
});
document.getElementById("headerText").innerHTML = "Bienvenido";

document.querySelector("#logoutButton").addEventListener("click", function (e) {
    window.location = 'logout.html';
}) 
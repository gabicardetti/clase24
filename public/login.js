function formSubmit(event) {
  event.preventDefault();

  const name = document.getElementById("fName").value;
  if (!name || name == "") {
    alert("El nombre no tiene que estar vacio");
    return;
  }
  const body = {
    name
  };

  const url = "/api/login";
  const request = new XMLHttpRequest();
  request.open("POST", url, true);
  request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  request.onload = function () {
    document.getElementById("fName").value = "";
    window.location = 'index.html';
  };

  request.onerror = function (e) { };

  request.send(JSON.stringify(body));
}
document.getElementById("form").addEventListener("submit", formSubmit);
